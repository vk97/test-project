import 'core-js';
import '../sass/style.scss';
import Slider from './components/slider';
import Menu from './components/header-menu';
import YourProject from './components/your-project';
import Scroll from './components/scroll';
import SocialPromo from './components/social-promo';

const slider = new Slider();
const menu = new Menu();
const yourProject = new YourProject();
const scroll = new Scroll();
const socialPromo = new SocialPromo();

slider.init();
menu.init();
yourProject.init();
scroll.init();
socialPromo.init();

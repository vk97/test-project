export default class Menu {
  constructor() {
    this.menuElement = document.querySelector(`.header-menu`)
    this.buttonToggleElement = document.querySelector(`.header-menu__toggle`);
    this.handleChangeMenuClass = this.handleChangeMenuClass.bind(this);
  }

  handleChangeMenuClass() {
    const menu = document.querySelector(`.header-menu__content`);
    this.buttonToggleElement.classList.toggle(`animate`);

    if (this.menuElement.classList.contains(`header-menu--closed`)) {
      this.menuElement.classList.remove(`header-menu--closed`);
      this.menuElement.classList.add(`header-menu--open`);
    } else {
      menu.classList.add(`close-animate`);
      this.menuElement.classList.add(`header-menu--closed`);
      this.menuElement.classList.remove(`header-menu--open`);
    }
  }

  mountedLinstener() {
    this.buttonToggleElement.addEventListener('click', this.handleChangeMenuClass);
  }

  init() {
    this.menuElement.classList.remove(`header-menu--nojs`);

    this.mountedLinstener();
  }
}

export default class Scroll {
  addAnim() {
    const scrollIcon = document.querySelector('.scroll__icon');

    setTimeout(function run() {

      scrollIcon.classList.add('down');

      setTimeout(function () {

        scrollIcon.classList.remove('down');
      }, 1000);
      setTimeout(run, 5000);

    }, 2000);
  }

  mountedListner() {
    const anchor = document.querySelector('.scroll__link');
    anchor.addEventListener('click', function (e) {
      e.preventDefault();

      const blockID = anchor.getAttribute('href').substr(1);

      document.getElementById(blockID).scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      });
    });
  }
  init() {
    this.mountedListner();
    this.addAnim();
  }
}

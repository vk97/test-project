import * as Swiper from '../swiper/dist/js/swiper';

export default class SliderGallary {
  constructor() {
    this.sliderGallary = null;
  }

  destroy() {
    if (this.sliderGallary) {
      for (const slider of this.sliderGallary) {
        slider.destroy();
      }

      this.sliderGallary = null;
    }
  }

  init() {
     this.sliderGallary = new Swiper('.gallary-slider', {
      loop: true,
      navigation: {
        nextEl: '.gallary-slider__button-next',
        prevEl: '.gallary-slider__button-prev',
      },
      autoHeight: true,
    });
  }
}


const TabItemSelector = '.tabs__link';
const ContentItemSelector = '.tab-panes__item';

const INIMATION_TIME = 400;

export default class Tabs {
  constructor(tabNode, onBackgroundChange) {
    this.tabs = [];
    this.activeTab = null;
    this.onBackgroundChange = onBackgroundChange;

    this.initFromHtml(tabNode);
    this.activateTab(this.tabs[0]);

  }

  initFromHtml(tabNode) {
    const headers = tabNode.querySelectorAll(TabItemSelector);
    const contents = tabNode.querySelectorAll(ContentItemSelector);

    headers.forEach((header, i) => {
      this.registerTab(header, contents[i]);
    });
  }

  registerTab(header, content) {
    const tab = new TabItem(header, content);
    tab.onActivate(() => this.activateTab(tab));
    this.tabs.push(tab);
  }

  addAnimationHide(newTab) {
    this.activeTab.content ? this.activeTab.content.classList.toggle('fade-out', true) : ``;

    setTimeout(() => {
      this.activeTab.content ? this.activeTab.content.classList.toggle('fade-out', false) : ``;
      this.activeTab.setActive(false);
      this.activeTab = newTab;
      this.activeTab.setActive(true);
      this.onBackgroundChange(newTab);
    }, INIMATION_TIME);
  }

  activateTab(tabItem) {
    if (this.activeTab) {
      this.addAnimationHide(tabItem);

      return;
    }

    this.activeTab = tabItem;
    this.activeTab.setActive(true);
    this.onBackgroundChange(tabItem);
  }

  getActiveTabs() {
    return this.activeTab;
  }

}

const ActiveTabHeaderClass = 'tabs__link--active';
const ActiveTabContentClass = 'tab-panes__item--active';

class TabItem {
  constructor(header, content) {
    this.header = header;
    this.content = content;
  }
  onActivate(action) {
    this.header.addEventListener('click', () => action(this));
  }
  setActive(value) {
    this.header.classList.toggle(ActiveTabHeaderClass, value);
    if (this.content) {
      this.content.classList.toggle(ActiveTabContentClass, value);
      this.content.classList.toggle('fade-in', value);
    }
  }
}

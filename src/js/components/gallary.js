
import Masonry from 'masonry-layout'
import imagesLoaded from 'imagesloaded';

export default class Galleries {
  constructor() {
    this.galleries = [];
  }

  destroy() {
    this.galleries.map((gallary) => gallary.destroy());
  }

  init() {
    const gallaryElements = document.querySelectorAll('.gallary-slider__wrapper');

    gallaryElements.forEach((gallary) => {
      const images = gallary.querySelectorAll('.gallary-slider__image');
      imagesLoaded(images, () => {
        this.galleries.push(new Masonry(gallary, {
          itemSelector: '.gallary-slider__slide',
          gutter: 40,
        }));
      });
    })
  }
}

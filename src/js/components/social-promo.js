export default class SocialPromo {
  addAnim() {
    const socialElements = document.querySelectorAll('.social-promo-lists__item');

    socialElements.forEach((element) => {
      setTimeout(function run() {

        element.classList.add('down');

        setTimeout(function () {

          element.classList.remove('down');
        }, 1000);
        setTimeout(run, 5000);

      }, 3000);
    })
  }

  init() {
    this.addAnim();
  }
}

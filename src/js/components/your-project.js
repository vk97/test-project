import Tabs from './tabs';
import SliderGallary from './slider-gallary';
import Galleries from './gallary';
import {SIZE_WIDTH} from '../const';

export default class YourProject {
  constructor() {
    this.tabsComponent = null;
    this.sliderComponent = new SliderGallary();
    this.galleriesComponent = new Galleries();
    this.yourProjectElement = document.querySelector('.your-project');
    this.handleBackgroundChange = this.handleBackgroundChange.bind(this);
  }

  setBackground({header, content}) {
    if (content || header) {
      const imageBackground = content ? content.querySelector('.gallary-slider__image').attributes.src.value : '';
      this.yourProjectElement.style.background = `url(${imageBackground}) #363636 no-repeat 50%`;
      this.yourProjectElement.style.backgroundSize = `cover`;
    } else {
      this.yourProjectElement.style.background = `transparent`;
    }
  }

  handleBackgroundChange(activeTab) {
    const {header, content} = activeTab;

    if (document.body.clientWidth >= SIZE_WIDTH.DESKTOP) {
      this.sliderComponent.destroy();
      this.galleriesComponent.init();

      if (content || header) {
        this.setBackground(activeTab);
      } else {
        const tab = this.tabsComponent.getActiveTabs();
        this.setBackground(tab);
      }
    } else {
      this.sliderComponent.init();
      this.galleriesComponent.destroy();
      this.setBackground(false);
    }

  }

  init() {
    const tabsContainerElement = document.querySelector('.tab-container');

    this.tabsComponent = new Tabs(tabsContainerElement, this.handleBackgroundChange);

    window.addEventListener('resize', this.handleBackgroundChange);
  }
}

import * as Swiper from '../swiper/dist/js/swiper';
export default class Slider {
  constructor() {
    this.handleChangeNumberSlide = this.handleChangeNumberSlide.bind(this);
  }

  getElemetsPagination(sliderElement, classSlide) {
    const slideElements = sliderElement.querySelectorAll(classSlide);

    if (slideElements.length) {
      return Array.from(slideElements).map((element, index) => {
        const isDataNoEmpty = element.hasAttribute(`data-slide-name`);

        if (isDataNoEmpty) {
          return element.attributes['data-slide-name'].value;
        }

        return `Slide ${++index}`;
      });
    }

    return ``;
  }

  initBaseSlider() {
    const sliderEmelents = document.querySelectorAll(`.slider-base`);

    sliderEmelents.forEach((sliderElement) => {
      const variantPaginations = this.getElemetsPagination(sliderElement, `.slider__slide`);

      const swiperOut = new Swiper(sliderElement, {
        pagination: {
          el: '.swiper-pagination--bottom, .swiper-pagination-top--base',
          clickable: true,
          renderBullet: function (index, className) {
            return '<a href="#!" class="' + className + '">' + (
              variantPaginations[index]
            ) + '</a>';
          },
        },
        loop: true,
        navigation: {
          nextEl: '.slider-next-btn',
          prevEl: '.slider-prev-btn',
        },

        autoHeight: true,
      });

      const swiperIn = new Swiper('.slider-gallary', {
        loop: true,
        navigation: {
          nextEl: '.swiper-button-in-next',
          prevEl: '.swiper-button-in-prev',
        },
        breakpointsInverse: true,
        breakpoints: {
          900: {
            slidesPerView: 'auto',
            spaceBetween: 20,
          },
        },
      });

    });
  }

  handleChangeNumberSlide() {
    const sixStepsElement = document.querySelector('.six-steps');
    const countSlides = sixStepsElement.querySelectorAll('.slider-six-steps-slide:not(.swiper-slide-duplicate)').length;
    const allSlidesElement = sixStepsElement.querySelectorAll('.slider-six-steps-slide');

    allSlidesElement.forEach((element) => {
      const elementNumber = element.querySelector('.slider-six-steps-slide__number');
      let slideNumber = element.attributes['data-swiper-slide-index'].value;
      Number(slideNumber);
      elementNumber.innerText = `${++slideNumber}/${countSlides}`;
    })
  }

  initStepToTheGoalSlider() {
    const sliderElement = document.querySelector(`.slider-six-steps`);

    const variantPaginations = this.getElemetsPagination(sliderElement, `.slider-six-steps-slide`);

      const swiper = new Swiper(sliderElement, {
        pagination: {
          el: '.swiper-pagination-bottom--six-steps, .swiper-pagination-top--six-steps',
          clickable: true,
          renderBullet: function (index, className) {
            return '<div class="slider-six-steps__pagination-button '+ className +'"><span>' + ((index + 1).toString().padStart(2, "0")) + '</span><a href="#!" class="">' + (
              variantPaginations[index]
            ) + '</a></div>';
          },
        },
        loop: true,
        navigation: {
          nextEl: '.slider-next-btn',
          prevEl: '.slider-prev-btn',
        },
        on: {
          init: this.handleChangeNumberSlide,
        },
        autoHeight: true,
      });

    swiper.on('init', this.handleChangeNumberSlide);
  }

  init() {
    this.initBaseSlider();
    this.initStepToTheGoalSlider();
  }
}
